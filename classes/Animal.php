<?php

abstract class Animal
{
    protected $name;
    protected $age;

    public abstract function getSpeciesName(): string;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        if ($age > 0) {
            $this->age = $age;
        }
        return $this;
    }

    public function manger(): self
    {
        echo "Je mange !";
        return $this;
    }

    public function dormir(): self
    {
        echo "Je dors !";
        return $this;
    }
}
