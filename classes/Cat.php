<?php

class Cat extends Animal
{
    private $isHunting;

    public function isHunting(): bool
    {
        return $this->isHunting;
    }

    public function setHunting(bool $hunting): self
    {
        $this->isHunting = $hunting;
        return $this;
    }

    public function meow(): string
    {
        return "Meeeow !";
    }

    public function getSpeciesName(): string
    {
        return "Felix catus";
    }
}