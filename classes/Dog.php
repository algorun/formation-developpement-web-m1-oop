<?php

class Dog extends Animal
{
    private $speed;
    private $owner;

    public function __construct(string $name, int $age, int $speed)
    {
        $this->setName($name);
        $this->setAge($age);
        $this->setSpeed($speed);
    }

    public function __toString(): string
    {
        return "Je m'appelle " . $this->name . " et j'ai " . $this->age . " ans !<br>";
    }

    public function __clone()
    {
        $this->owner = clone $this->owner;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function setSpeed(int $speed): self
    {
        $this->speed = $speed;
        return $this;
    }

    public function bark(): string
    {
        return "Waf waf !";
    }

    public function getSpeciesName(): string
    {
        return "Canis lupus familiaris";
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(PetOwner $owner)
    {
        $this->owner = $owner;
    }
}