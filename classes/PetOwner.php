<?php

class PetOwner
{
    private $name;
    private $firstname;
    private $pets = array();

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getPets(): array
    {
        return $this->pets;
    }

    public function getPet(int $position): ?Animal
    {
        if (count($this->pets) < $position && $position > 0) {
            return $this->pets[$position];
        }

        return null;
    }


    public function addPet(Animal $animal): self
    {
        foreach ($this->pets as $pet) {
            if ($pet->getName() === $animal->getName()) {
                return $this;
            }
        }

        $this->pets[] = $animal;
        return $this;
    }
}