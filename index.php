<?php

header('Content-type: text/html');
spl_autoload_register(function($class) {
    include 'classes/' . $class . '.php';
});

$monChien = new Dog("Médor", 15, 10);

$monChat = new Cat();
$monChat->setName("Garfield");
$monChat->setAge(4);
$monChat->setHunting(true);

echo "Mon chien s'appelle " . $monChien->getName() . " et il dit : " . $monChien->bark() . "<br>";
echo "Mon chien est de la famille des " . $monChien->getSpeciesName() . "<br>";
echo "Mon chat s'appelle " . $monChat->getName() . " et il dit : " . $monChat->meow() . "<br>";

echo $monChien;

$leMaitre = new PetOwner();
$leMaitre->setFirstname("Jean")->setName("Dupont");
$monChien->setOwner($leMaitre);

$monChien2 = clone $monChien;

$monChien2->getOwner()->setName("Doe")->setFirstname("John");

var_dump($monChien);
var_dump($monChien2);